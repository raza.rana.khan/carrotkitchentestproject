import { Resolver, Query, Mutation, Args} from '@nestjs/graphql';

@Resolver()
export class RecipeResolver {
  //dummy data
  RecipesFromMemory = [
    { id: 0, name: 'Chocolate chip cookies' },
  ];


  //query as defined in schema 
  @Query()
  recipes() {
    return this.RecipesFromMemory;
  }

  //mutation as defined in schema
  @Mutation()
  createRecipe(@Args('name') name: string) {
    //creating id of new recipe 
    const id = this.RecipesFromMemory.length;
    const newRecipe = { id, name };
    //adding new recpe to the recipes from memory array
    this.RecipesFromMemory.push(newRecipe);
    return newRecipe;
  }


}