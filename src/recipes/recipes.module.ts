import { Module } from '@nestjs/common';
import { RecipeResolver } from './recipes.resolver';

@Module({
    //adding recipe resolver to the module
    providers: [RecipeResolver],
    exports: [RecipeResolver],
})
export class RecipesModule {}
