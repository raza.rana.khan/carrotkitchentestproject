# Carrot Kitchen Test

Task: Make a small NestJS & GraphQL server with 1 query and 1 mutation.

Solution:

-> Installed NestJS and created a new project

-> Installed Graphql and imported module 

-> Created Schema of Recipe, Query and Mutation

-> Created module Recipe 

-> Created Recipe Resolver and dummy recipe data

-> Created Receipe query in resolver according to the one defined in schema 


![Receipes List on query run](receipeList1.png)

-> created mutation as defined in the schema 

![Receipes List on query run](addNewReceipe.png)

On adding new Receipe the receipe array is updated 

![Receipes List on query run](receipeList2.png)
